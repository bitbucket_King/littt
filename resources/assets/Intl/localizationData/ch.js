export default {
  locale: 'en',
  messages: {
    siteTitle: 'MERN Starter Blog',
    addPost: 'Add Post',
    switchLanguage: 'Switch Language',
    twitterMessage: 'We are on Twitter',
    by: 'By',
    deletePost: 'Delete Post',
    createNewPost: 'Create new post',
    authorName: 'Author\'s Name',
    postTitle: 'Post Title',
    postContent: 'Post Content',
    submit: 'Submit',
    comment: `user {name} {value, plural,
    	  =0 {does not have any comments}
    	  =1 {has # comment}
    	  other {has # comments}
    	}`,
    HTMLComment: `user <b style='font-weight: bold'>{name} </b> {value, plural,
    	  =0 {does not have <i style='font-style: italic'>any</i> comments}
    	  =1 {has <i style='font-style: italic'>#</i> comment}
    	  other {has <i style='font-style: italic'>#</i> comments}
    	}`,
    nestedDateComment: `user {name} {value, plural,
    	  =0 {does not have any comments}
    	  =1 {has # comment}
    	  other {has # comments}
      } as of {date}`,

    // Top Menus
    "Language": `语言`,
    "Products": `产品`,
    "Stay In Touch": `保持联系`,
    "News & Press": `新闻 & 压机`,
    "Pre-Order": `预购`,
    "Contact Us": `联系我们`,

    // Sub Menus
    "Back_LiTTT": `返回 LiTTT<1>TM</1>`,
    "Support Us": `支持我们`,
    "Email Us": `Email Us`,
    "Discord Chat": `Discord Chat`,
    "Facebook Us": `Facebook Us`,
    "Tweet Us": `Tweet Us`,
    "RGB+W LEDs": `RGB+W LEDs`,
    "LiTTT_Snap": `LiTTT<1>TM</1> Snap!`,
    "LiTTT_Hub": `LiTTT<1>TM</1> Hub<1>TM</1>`,
    "Press Releases": `新闻稿`,
    "Dev Blog": `开发博客`,
    "Newsletter": `通讯`,


    // Newsletter
    "NewsletterPlaceholder": "您的电子邮件地址",
    "NewsletterTitle": "输入您的电子邮件, 我们会告诉你的",
    "NewsletterBtn": "立即注册",
    "NewsletterSuccess": `<1>谢谢!</1> <2>你已成功地加入我们的认购人名单.</2>`,

    // Footer Links
    "Reserve Today": `今日储备`,
    "Pre-Order Now": `现在预购`,
    "All Products": `所有产品`,
    "Discount": "折扣",
    "Sign-Up for Our Newsletter": "注册我们的通讯",
  },
};
