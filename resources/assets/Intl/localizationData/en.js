export default {
  locale: 'en',
  messages: {
    siteTitle: 'MERN Starter Blog',
    addPost: 'Add Post',
    switchLanguage: 'Switch Language',
    twitterMessage: 'We are on Twitter',
    by: 'By',
    deletePost: 'Delete Post',
    createNewPost: 'Create new post',
    authorName: 'Author\'s Name',
    postTitle: 'Post Title',
    postContent: 'Post Content',
    submit: 'Submit',
    comment: `user {name} {value, plural,
    	  =0 {does not have any comments}
    	  =1 {has # comment}
    	  other {has # comments}
    	}`,
    HTMLComment: `user <b style='font-weight: bold'>{name} </b> {value, plural,
    	  =0 {does not have <i style='font-style: italic'>any</i> comments}
    	  =1 {has <i style='font-style: italic'>#</i> comment}
    	  other {has <i style='font-style: italic'>#</i> comments}
    	}`,
    nestedDateComment: `user {name} {value, plural,
    	  =0 {does not have any comments}
    	  =1 {has # comment}
    	  other {has # comments}
      } as of {date}`,
    // Top Menus
    "Language": `Language`,
    "Products": `Products`,
    "Stay In Touch": `Stay In Touch`,
    "News & Press": `News & Press`,
    "Pre-Order": `Pre-Order`,
    "Contact Us": `Contact Us`,

    // Sub Menus
    "Back_LiTTT": `Back LiTTT<1>TM</1>`,
    "Support Us": `Support Us`,
    "Email Us": `Email Us`,
    "Discord Chat": `Discord Chat`,
    "Facebook Us": `Facebook Us`,
    "Tweet Us": `Tweet Us`,
    "RGB+W LEDs": `RGB+W LEDs`,
    "LiTTT_Snap": `LiTTT<1>TM</1> Snap!`,
    "LiTTT_Hub": `LiTTT<1>TM</1> Hub<1>TM</1>`,
    "Press Releases": `Press Releases`,
    "Dev Blog": `Dev Blog`,
    "Newsletter": `Newsletter`,

    // Newsletter
    "NewsletterPlaceholder": "your email address",
    "NewsletterTitle": "Enter your Email and We'll keep you posted!",
    "NewsletterBtn": "Sign Up Now",
    "NewsletterSuccess": `<1>谢谢!</1> <2>You have successfully joined our subscriber list.</2>`,

    // Footer
    "Reserve Today": `Reserve Today`,
    "Pre-Order Now": `Pre-Order Now`,
    "All Products": `All Products`,
    "Discount": "Discount",
    "Sign-Up for Our Newsletter": "Sign-Up for Our Newsletter",
  },
};
