import { combineReducers } from 'redux'

import intl from '../modules/Intl/IntlReducer'
import auth from '../modules/auth/store/reduer'
import user from '../modules/user/store/reducer'
import articles from '../modules/article/store/reducer'

export default combineReducers({ intl, auth, user, articles })
