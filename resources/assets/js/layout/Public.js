//import libs
import React from 'react'
import PropTypes from 'prop-types'

// import components
import Navigation from '../common/navigation'
import ScrollTop from '../common/scroll-top'
import Footer from '../common/footer'

const containerStyle = {
  paddingTop: '3.5rem',
}

const displayName = 'Public Layout'
const propTypes = {
  children: PropTypes.node.isRequired,
}

function PublicLayout({ children }) {
  return <div style={containerStyle}>
    <Navigation height={80}/>
    <div style={{ minHeight: '65vh', marginTop: -30}}>
      { children }
      <ScrollTop />
    </div>
    <Footer height={`13vh`}/>
  </div>
}

PublicLayout.dispatch = displayName
PublicLayout.propTypes = propTypes

export default PublicLayout
