//import libs
import React from 'react'
import PropTypes from 'prop-types'

// import components
import Navigation from '../common/navigation'
import ScrollTop from '../common/scroll-top'
import Footer from '../common/footer'

const containerStyle = {
  paddingTop: '3.5rem',
}

const displayName = 'Private Layout'
const propTypes = {
  children: PropTypes.node.isRequired,
}

function PrivateLayout({ children }) {
  return <div style={containerStyle}>
    <Navigation height={80}/>
    <main style={{ minHeight: '65vh', marginTop: -30}}>
      { children }
      <ScrollTop />
    </main>
    <Footer height={`calc(100vh - 80vh - 80px) !important;`}/>
  </div>
}

PrivateLayout.dispatch = displayName
PrivateLayout.propTypes = propTypes

export default PrivateLayout
