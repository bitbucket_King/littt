import { localizationData } from '../../../Intl/setup';
import i18n from './../../i18n';

// Export Constants
// when using react-intl 
export const INTL_SWITCH_LANGUAGE = 'INTL_SWITCH_LANGUAGE';
// when using react-i18n 
export const I18N_SWITCH_LANGUAGE = 'I18N_SWITCH_LANGUAGE';

export function switchLanguage(newLang) {
  return {
    type: INTL_SWITCH_LANGUAGE,
    ...localizationData[newLang],
  };
}

export function changeLanguage(lng) {
  i18n.changeLanguage(lng);
  return {
    type: I18N_SWITCH_LANGUAGE,
    lng,
  };
}
