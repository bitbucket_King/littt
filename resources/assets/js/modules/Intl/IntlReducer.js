import { enabledLanguages, localizationData } from '../../../Intl/setup';
import { INTL_SWITCH_LANGUAGE, I18N_SWITCH_LANGUAGE } from './IntlActions';

const initLocale = global.navigator && global.navigator.language || 'en';

const initialState = {
  locale: initLocale.substr(0, 2),
  enabledLanguages,
  ...(localizationData[initLocale.substr(0, 2)] || {}),
};

const IntlReducer = (state = initialState, action) => {
  switch (action.type) {
    case INTL_SWITCH_LANGUAGE: {
      const { type, ...actionWithoutType } = action; // eslint-disable-line
      return { ...state, ...actionWithoutType };
    }

    case I18N_SWITCH_LANGUAGE: {
      const { type, ...actionWithoutType } = action; // eslint-disable-line
      return { ...state, ...actionWithoutType };
    }

    default:
      return state;
  }
};

export default IntlReducer;
