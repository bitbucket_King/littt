import React, { Component } from "react"
import PropTypes from "prop-types"
import Iframe from 'react-iframe'

const containerStyle = {
  position: 'absolute',
  top: 0,
  width: '100vw'
}

class Page extends Component {
  static displayName = "GatsbyHomePage"
  static propTypes = {
  }

  render() {
    return <div style={containerStyle}>
      {/* https://vibrant-boyd-f714f0.netlify.com */}
      <Iframe url="http://localhost:8000"
        width="100%"
        height="100vh"
        id="myId"
        className="myClassname"
        display="initial"
        position="relative"
        allowFullScreen/>
    </div>
  }
}

export default Page
