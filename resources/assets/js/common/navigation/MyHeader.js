// import libs
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import MediaQuery from 'react-responsive';
import { compose } from 'redux'
import { connect } from 'react-redux'

// import components
import { Collapse } from 'reactstrap'
import LanguageSwitcher from './../language-switcher/LanguageSwitcher'
import SubMenu1 from './sub/SubMenu1'
import SubMenu2 from './sub/SubMenu2'
import SubMenu3 from './sub/SubMenu3'
import SubMenu4 from './sub/SubMenu4'
import SubMenu5 from './sub/SubMenu5'

import MenuList from "@material-ui/core/MenuList";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import MenuItem from '@material-ui/core/MenuItem';

// import render from '../articles/listing/components/Article';
import { withStyles } from "@material-ui/core/styles";
import { Trans } from 'react-i18next';
import { FormattedMessage, FormattedHTMLMessage } from 'react-intl';

import i18n from './../../i18n';
import { changeLanguage } from './../../modules/Intl/IntlActions'
import './MyHeader.css' ;

const baseUrl = "/img/"

const commonMenus = [
  {icon: `/img/icons/Kickstarter.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans i18nKey="Back_LiTTT">Back LiTTT<sup style={{fontSize: 'medium'}}>TM</sup></Trans></span>), to: "https://www.kickstarter.com/projects/littt/1883533304?ref=599314&token=14b04d38"},
  {icon: `/img/icons/Support.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Support Us</Trans></span>)}
]

const contactMenus= [
  {icon: `/img/icons/Mail.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Email Us</Trans></span>)},
  {icon: `/img/icons/Discord.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Discord Chat</Trans></span>)},
  {icon: `/img/icons/Facebook.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Facebook Us</Trans></span>)},
  {icon: `/img/icons/Twitter.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Tweet Us</Trans></span>)}
]

const preOrderMenus= [
  //<FormattedHTMLMessage id="LiTTT_Snap" />
  {icon: `/img/icons/Device.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>RGB+W LEDs</Trans></span>), to: "https://www.kickstarter.com/projects/littt/1883533304?ref=599314&token=14b04d38" },
  {icon: `/img/icons/Device.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans i18nKey="LiTTT_Snap" >LiTTT<sup style={{fontSize: 'medium'}}>TM</sup> Snap!</Trans></span>), to: "https://www.kickstarter.com/projects/littt/1883533304?ref=599314&token=14b04d38" },
  {icon: `/img/icons/Device1.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans i18nKey="LiTTT_Hub" >LiTTT<sup style={{fontSize: 'medium'}}>TM</sup> Hub<sup style={{fontSize: 'medium'}}>TM</sup></Trans></span>), to: "https://www.kickstarter.com/projects/littt/1883533304?ref=599314&token=14b04d38" },
]

const newsletterAction = () => { ml_webform_1088090('show') }

const newsPressMenus= [
  {icon: `/img/icons/Release.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Press Releases</Trans></span>), to: 'http://vibrant-boyd-f714f0.netlify.com/category/press-releases'},  // https://vibrant-boyd-f714f0.netlify.com/category/press-releases
  {icon: `/img/icons/Blog.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Dev Blog</Trans></span>), to: 'http://vibrant-boyd-f714f0.netlify.com/category/dev-blog' },  // https://vibrant-boyd-f714f0.netlify.com/category/dev-blog
  {icon: `/img/icons/Mail.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Newsletter</Trans></span>), onClick: newsletterAction },
]

const stayInTouchMenus= [
  {icon: `/img/icons/Mail.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Email Us</Trans></span>)},  
  {icon: `/img/icons/Discord.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Discord Chat</Trans></span>)},
  {icon: `/img/icons/Facebook.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Facebook Us</Trans></span>)},
  {icon: `/img/icons/Twitter.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>Tweet Us</Trans></span>)}
]

const productsMenus= [
  {icon: `/img/icons/Device.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans>RGB+W LEDs</Trans></span>), to: "https://www.kickstarter.com/projects/littt/1883533304?ref=599314&token=14b04d38" },
  {icon: `/img/icons/Device.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans  i18nKey="LiTTT_Snap">LiTTT<sup style={{fontSize: 'medium'}}>TM</sup> Snap!</Trans></span>), to: "https://www.kickstarter.com/projects/littt/1883533304?ref=599314&token=14b04d38" },
  {icon: `/img/icons/Device1.png`, label: (<span>&nbsp;&nbsp;&nbsp;<Trans i18nKey="LiTTT_Hub" >LiTTT<sup style={{fontSize: 'medium'}}>TM</sup> Hub<sup style={{fontSize: 'medium'}}>TM</sup></Trans></span>), to: "https://www.kickstarter.com/projects/littt/1883533304?ref=599314&token=14b04d38" }
]

const styles = theme => ({
  root: {
    display: "flex"
  },
  paper: {
    marginRight: theme.spacing.unit * 2,
  },
  topMenu: {
    color: 'white',
    fontSize: 23,
    fontWeight: 300,
    textTransform: 'none',
    padding: '20px 50px',
    [theme.breakpoints.down('md')]: {
      fontSize: 22,
      color: 'black',
      padding: 15,
      width: '100%',
    },
    outline: 'none !important;'
  },  
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  langBtn: {
    height: '100%',
    width: '10vw',
    maxWidth: 120,
    minWidth: 50
  }
});
class MyHeader extends Component {

  // changeLanguage = (code) => {
  //   i18n.changeLanguage(code);
  //   this.props.dispatch(changeLanguage(code));
  // }

  state = {
    auth: true,
    lanugage: 'English',
    flag: `${baseUrl}icons/flag/uk.svg`,
    open: false,
    mobileopen: false
  };

  changeLanguage = (lanugage, code, flag) => {
      this.props.dispatch(changeLanguage(code));
      this.setState({ lanugage: lanugage, flag: flag });
  }

  handleToggle = (event) => {
      event.preventDefault();
      this.setState(state => ({ open: !state.open }));
      {
          this.state.open?
          document.getElementById("border3").removeAttribute("class", "borderBottom3"): document.getElementById("border3").setAttribute("class", "borderBottom3");
      }
      {
          this.state.open && document.getElementById("footer")?
          document.getElementById("footer").removeAttribute("class", "fog"): document.getElementById("footer").setAttribute("class", "fog");
      }
  };

  handleClose = event => {
      if (this.anchorEl.contains(event.target)) {
          return;
      }
      document.getElementById("border3").removeAttribute("class", "borderBottom3");
      document.getElementById("footer") && document.getElementById("footer").removeAttribute("class", "fog");
      this.setState({ open: false });
  };

  handleMobileToggle = () => {
    this.setState(state => ({ mobileopen: !state.mobileopen }));
    {
        this.state.mobileopen  && document.getElementById("footer")?
        document.getElementById("footer").removeAttribute("class", "fog"): document.getElementById("footer").setAttribute("class", "fog");
    }
  };

  handleMobileClose = event => {
      if (this.anchorMobileEl.contains(event.currentTarget)) {
          return;
      }
      document.getElementById("footer") && document.getElementById("footer").removeAttribute("class", "fog");
      this.setState({ mobileopen: false });
  };

  render() {
    const { classes } = this.props;
    const { open, mobileopen } = this.state;

    return  (
      <Collapse className="navbar-collapse navbar-dark" isOpen={this.props.showNavigation}>
          <div className="mobile-menu-bg">
              <ul className="navbar-nav ml-auto">
                <MediaQuery query="(max-width: 991px)">
                  {/* <LanguageSwitcher  menuTitle="Language" changeLanguage={this.changeLanguage}/> */}
                  <div style={{ 
                        background: "linear-gradient( to left, rgba(169, 46, 145, 1) 0%, rgba(181, 198, 52, 1) 100% ) left bottom #fff no-repeat",
                        backgroundSize: "100% 1px"
                  }}>
                    <Button
                        buttonRef={node => { this.anchorMobileEl = node; }}
                        aria-owns={mobileopen ? "menu-list-grow" : null}
                        aria-haspopup="true"
                        onClick={this.handleMobileToggle}
                        className={classes.topMenu}
                    >
                        <Trans i18nKey="Language"></Trans>
                    </Button>
                    <Collapse isOpen={mobileopen} timeout="auto" unmountOnExit 
                        style={{
                            background: "linear-gradient(to left, rgb(169, 46, 145) 0%, rgb(181, 198, 52) 100%) left top / 100% 4px no-repeat rgb(255, 255, 255)",
                            boxShadow: 'inset 0px 0px 8px 0px rgba(125,125,125,1)',
                        }}>
                        {/* <ClickAwayListener onClickAway={this.handleMobileClose}> */}
                            <MenuList style={{display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                <MenuItem onClick={(e) => { this.handleMobileClose(e); this.changeLanguage('English', 'en',  `${baseUrl}icons/flag/uk.svg`)}}>
                                  <img src={`${baseUrl}icons/flag/uk.svg`} style={{width: 25}}/>&nbsp;&nbsp;&nbsp; English
                                </MenuItem>
                                <MenuItem onClick={(e) => { this.handleMobileClose(e); this.changeLanguage('Chinese', 'ch', `${baseUrl}icons/flag/china.svg`)}}>
                                  <img src={`${baseUrl}icons/flag/china.svg`} style={{width: 25}}/>&nbsp;&nbsp;&nbsp; Chinese
                                </MenuItem>
                            </MenuList>              
                        {/* </ClickAwayListener> */}
                    </Collapse>
                  </div>
                </MediaQuery>
                <SubMenu1 menuTitle="Products" subMenus={productsMenus} commonMenus={commonMenus}/>
                <SubMenu2 menuTitle="Stay In Touch" subMenus={stayInTouchMenus} commonMenus={commonMenus}/>
                <SubMenu3 menuTitle="News & Press" subMenus={newsPressMenus} commonMenus={commonMenus}/>
                <SubMenu4 menuTitle="Pre-Order" subMenus={preOrderMenus} commonMenus={commonMenus}/>
                <SubMenu5 menuTitle="Contact Us" subMenus={contactMenus} commonMenus={commonMenus}/>
                <MediaQuery query="(min-width: 992px)">
                  {/* <LanguageSwitcher menuTitle="Language" changeLanguage={this.changeLanguage}/> */}
                  <div>
                      <Button
                          buttonRef={node => { this.anchorEl = node; }}
                          className={classes.langBtn}
                          aria-owns={open ? 'menu-list-grow' : undefined}
                          aria-haspopup="true"
                          onClick={this.handleToggle}
                      >
                          <img src={this.state.flag} style={{marginRight: 20, width: 25}}/> <img src={`${baseUrl}icons/flag/drop-down.svg`} width="15%"/>
                      </Button>
                      <Popper open={open} anchorEl={this.anchorEl} transition disablePortal placement="bottom-end">
                          {({ TransitionProps, placement }) => (
                          <Grow
                            {...TransitionProps} id="menu-list-grow"
                            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                          >
                              <Paper style={{width: 140}}>
                                  <ClickAwayListener onClickAway={this.handleClose}>
                                  <MenuList>
                                      <MenuItem onClick={(e) => { this.handleClose(e); this.changeLanguage('English', 'en',  `${baseUrl}icons/flag/uk.svg`)}}>
                                        <img src={`${baseUrl}icons/flag/uk.svg`} style={{width: 25}}/>&nbsp;&nbsp;&nbsp; English
                                      </MenuItem>
                                      <MenuItem onClick={(e) => { this.handleClose(e); this.changeLanguage('Chinese', 'ch', `${baseUrl}icons/flag/china.svg`)}}>
                                        <img src={`${baseUrl}icons/flag/china.svg`} style={{width: 25}}/>&nbsp;&nbsp;&nbsp; Chinese
                                      </MenuItem>
                                  </MenuList>
                                  </ClickAwayListener>
                              </Paper>
                          </Grow>
                          )}
                      </Popper>
                  </div>
                </MediaQuery>
              </ul>
          </div>
      </Collapse>)
  }
}

MyHeader.displayName = 'MyHeader'
MyHeader.propTypes = {
  showNavigation: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
}

const mapStateToProps = state => {
  return {
    locale: state.intl.locale,
  }
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps)
)(MyHeader)


