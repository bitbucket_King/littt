import React, { Component } from "react";
import Slider from "react-slick";
import MediaQuery from 'react-responsive';
import { withStyles } from '@material-ui/core/styles';

const baseUrl = "/img/"
const styles = theme => ({
  banner: {
    width: '100vw',
    height: '80vh',
    objectFit: 'none'
  },
  dot: {
    backgroundColor: 'rgb(255,255,255, .3) !important',
    width: '5vw !important',
    maxWidth: 60,
    height: '0px !important',
    borderRadius: '5px !important',
    padding: '3px !important',
    border: '0.5px solid rgba(0,0,0, .2) !important'
  },
  nextArrow: {
    display: "block",  
    background: "url(/img/icons/next.png) no-repeat",
    backgroundSize: "100%",
    width: 60,
    height: 75,
    [theme.breakpoints.between('sm', 'md')]: {
      width: 50,
      height: 62,
      right: '15px !important;'
    },
    [theme.breakpoints.between('xs', 'sm')]: {
      width: 40,
      height: 50,
      right: '10px !important;'
    },
    [theme.breakpoints.down('xs')]: {
      width: 35,
      height: 45,
      right: '10px !important;'
    },
    '&:before': {
      content: "'' !important",
      background: "url(/img/icons/next.png) no-repeat !important"
    },
    '&:hover': {
      background: "url(/img/icons/next.png) no-repeat",
      backgroundSize: "100%",
    }
  },
  prevArrow: {
    display: "block",  
    background: "url(/img/icons/prev.png) no-repeat",
    backgroundSize: "100%",
    width: 60,
    height: 75,
    [theme.breakpoints.between('sm', 'md')]: {
      width: 50,
      height: 62,
      left: '15px !important;'
    },
    [theme.breakpoints.between('xs', 'sm')]: {
      width: 40,
      height: 50,
      left: '10px !important;'
    },
    [theme.breakpoints.down('xs')]: {
      width: 35,
      height: 45,
      left: '10px !important;'
    },
    '&:before': {
      content: "'' !important",
    },
    '&:hover': {
      background: "url(/img/icons/prev.png) no-repeat",
      backgroundSize: "100%",
    }
  }
});

const SampleNextArrow = withStyles(styles)
((props) => {
  const { classes, className, style, onClick } = props;
  return (
    <div
      className={`${className} ${classes.nextArrow}` }
      style={{ ...style }}
      onClick={onClick}
    />
  );
}) 

const SamplePrevArrow = withStyles(styles) 
((props) => {
  const { classes, className, style, onClick } = props;
  return (
    <div
      className={`${className} ${classes.prevArrow}`}
      style={{ ...style }}
      onClick={onClick}
    />
  );
})

class FullScreenCarousel extends Component {

  render() {
    const { classes } = this.props;
    const settings = {
      customPaging: function(i) {
        return (<a> <button className={classes.dot}></button> </a>)
      },
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true,
      arrows: true,
      dots: true,
      autoplay: true,
      autoplaySpeed: 6000,
      // dotsClass: "slick-dots slick-thumb",
      // draggable: false,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />
    };
    return (
      <div>
        <MediaQuery query="(min-width: 600px)">
          <Slider {...settings}>
            <div>
              <img src={`${baseUrl}/Carousels/1-d.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/2.gif`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/3-d.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/4-d.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/5-d.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/6.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/7.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/8-d.jpg`} className={classes.banner} />
            </div>
          </Slider>
        </MediaQuery>
        <MediaQuery query="(max-width: 600px)">
          <Slider {...settings}>
            <div>
              <img src={`${baseUrl}/Carousels/1-m.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/2.gif`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/3-m.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/4-m.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/5-m.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/6-m.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/7-m.jpg`} className={classes.banner} />
            </div>
            <div>
              <img src={`${baseUrl}/Carousels/8-m.jpg`} className={classes.banner} />
            </div>  
          </Slider>        
        </MediaQuery>
      </div>
    );
  }
}

export default withStyles(styles) (FullScreenCarousel)