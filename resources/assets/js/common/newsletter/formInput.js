import React, { Component } from "react";
import "./formInput.css";
import { withNamespaces } from 'react-i18next';

class FormInput extends Component {
  render() {
    const {t} = this.props
    return (
      <div className="divs ml-field-group ml-field-email ml-validate-email ml-validate-required">
        <input type="email" data-inputmask="" name="fields[email]" defaultValue="" placeholder={t('NewsletterPlaceholder')}/>
      </div>
    );
  }
}

export default withNamespaces() (FormInput);
