import React from "react";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core/styles";

const styles = {
    root: {
        padding: '12px 0px'
    },
    btnContainer: {
        position: 'relative'
    },
    btn: {
        backgroundImage: "linear-gradient(to right, rgb(181, 198, 52) 0%, rgb(169, 46, 145) 100%)", // this is different from BrightButton
        borderRadius: 30,
        color: 'rgb(0, 128, 200)',
        height: 46,
        letterSpacing: 0,
        margin: '0px, 20px',
        padding: 2,
        minWidth: 230,
        zIndex: 2,
        fontFamily: `Myriad Pro`,
        fontSize: 26,
        textTransform: `none`,
        boxShadow: '0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)',
    },
    btnSpan: {
        alignItems: "center",
        background: "black", // this is different from BrightButton
        borderRadius: 40,
        display: "flex",
        justifyContent: "center",
        transition: "background .5s ease",
        height: 42,
        width: "100%"
    },
    icon: {
        width: 65,
        height: 65,
        position: 'absolute',
        zIndex: 3,
        left: -27,
        top: -10,
        // transform: 'rotate(-10deg)',
        // boxShadow: '0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14), 0px 3px 1px -2px rgba(0, 0, 0, 0.12)'
    }
};

function IconLabelButtons({icon, label}) {
  return (
    <div style={styles.root}>
        <div style={styles.btnContainer} >
            {icon && <img style={styles.icon} src={icon}/>}
            <Button style={styles.btn}>        
                <span style={styles.btnSpan}>{label}</span>
            </Button>
        </div>        
    </div>
  );
}

IconLabelButtons.propTypes = {
  classes: PropTypes.object.isRequired,
  icon: PropTypes.string,
  label: PropTypes.element
};

export default withStyles(styles)(IconLabelButtons);
