import React, {Component} from 'react'
import PropTypes from 'prop-types'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import compose from 'recompose/compose';

import MyButton from '../button/MyButton';
import BlackButton from '../button/BlackButton';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import { Trans } from 'react-i18next';

const newsletterAction = () => { ml_webform_1088090('show') }
const footerMenus= [
  { label: (<span><Trans>Reserve Today</Trans></span>), onClick: newsletterAction },
  { label: (<span><Trans>Pre-Order Now</Trans></span>), href: "https://www.kickstarter.com/projects/littt/1883533304?ref=599314&token=14b04d38" },
  { label: (<span><Trans>All Products</Trans></span>), href: "https://www.kickstarter.com/projects/littt/1883533304?ref=599314&token=14b04d38" },
]

const displayName = 'Footer'
const propTypes = {
  height: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
}

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  dotWrapper: {
    font: "1.2rem 'Oswald', Arial, sans-serif",
    width: 800,
    [theme.breakpoints.between('sm', 'md')]: {
      width: 600,
    },
    [theme.breakpoints.between('xs', 'sm')]: {
      width: 500,
    },
    [theme.breakpoints.down('xs')]: {
      width: 300,
    },
    height: 120,
    textAlign: 'center',
    letterSpacing: 1,
    margin: '0 auto',
    backgroundSize: '100% 100%',
    backgroundImage: 'url(/img/panels/Panel2@2x.png)',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center top'
  },
  dotInnerWrapper: {
    position: 'relative',
    top: -30
  },
  paper: {
    height: 140,
    width: 100,
  },
  control: {
    padding: theme.spacing.unit * 2,
  },
  gridContainer: {
      paddingTop: '5vh',
      [theme.breakpoints.between('sm', 'md')]: {
        paddingTop: '0',
      },
      [theme.breakpoints.between('xs', 'sm')]: {
        paddingTop: '10vh',
      },
      [theme.breakpoints.down('xs')]: {
        paddingTop: '35vh',
      },
  },
  gridItem: {
    [theme.breakpoints.down('md')]: {
      paddingTop: '3.5vh'
    }
  },
  button: {
    margin: theme.spacing.unit,
    position: 'relative',
    top: -40
  },
  buttonWrapper: {
    width: 350,
    [theme.breakpoints.down('md')]: {
      width: 280,
    },
    height: 70,
    backgroundImage: "linear-gradient(#b5c634, transparent), linear-gradient(#a92e91, transparent)",
    MozBackgroundSize: "5px 100%",
    backgroundSize: "1px 100%",
    backgroundPosition: "0 0, 100% 0",
    backgroundRepeat: "no-repeat",
    WebkitBoxShadow: '0px -1px 2px 1px rgba(196,196,196,0.4)',
    MozBoxShadow: '0px -1px 2px 1px rgba(196,196,196,0.4)',
    boxShadow: '0px -1px 2px 2px rgba(196,196,196,0.2)',
    // outline: '3px solid white',
    textAlign: 'center'
  },
  input: {
    display: 'none',
  },
  textHorizontalGradient: {
    background: '-webkit-linear-gradient(left, rgb(181, 198, 52), rgb(169, 46, 145))',
    background: '-webkit-linear-gradient(left,  rgb(181, 198, 52), rgb(169, 46, 145))',
    background: '-o-linear-gradient(right,  rgb(181, 198, 52), rgb(169, 46, 145))',
    background: '-moz-linear-gradient(right,  rgb(181, 198, 52), rgb(169, 46, 145))',
    background: 'linear-gradient(to right,  rgb(181, 198, 52), rgb(169, 46, 145))',
    WebkitBackgroundClip: 'text',
    WebkitTextFillColor: 'transparent'
  },


});

class Footer extends Component { 
  constructor(props) {
    super(props)
    this.state = {
      spacing: '16',
    };
  }
  render() {
      const { classes } = this.props;
      const { spacing } = this.state;
      console.log("props width",this.props.width)
      return (<footer className="sticky-bottom py-3 bg-bright" style={{height: this.props.height, position: 'relative'}}>
                <div id="footer" style={{ position: 'absolute', width: '100%', top: -130 }}>
                    <div className={classes.dotWrapper}>
                      <div className={classes.dotInnerWrapper}>
                        <BlackButton label={<span className={classes.textHorizontalGradient}><Trans>Stay In Touch</Trans></span>} className={classes.button}/>
                        <div>
                          {
                            isWidthUp('md', this.props.width)?
                              (
                                <span>
                                  <span style={{color: 'red'}}>10% <Trans>Discount</Trans></span>
                                    <span style={{color: 'white'}}>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; <Trans>Sign-Up for Our Newsletter</Trans></span>                                  
                                </span>
                              )
                              :
                              (
                                <span>
                                  <span style={{color: 'red'}}>10% <Trans>Discount</Trans></span>
                                  <span style={{color: 'white', display: 'block'}}><Trans>Sign-Up for Our Newsletter</Trans></span>
                                </span>
                              )
                          }
                        </div>                    
                      </div>
                    </div>
                </div>
 
                <Grid container className={classes.root} style={{height: '10vh'}}  alignItems="center">
                    <Grid item xs={12}>
                      <Grid container className={classes.gridContainer} justify="space-around">
                          {
                            footerMenus.map((menu, i) => {
                              return <Grid key={`footer${i+1}`} item className={classes.gridItem} >                            
                                <div className={classes.buttonWrapper}>
                                  <div className={classes.button}>
                                    <MyButton onClick={menu.onClick} href={menu.href} label={menu.label} />
                                  </div>
                                </div>
                              </Grid>
                            })
                          }
                      </Grid>
                    </Grid>
                </Grid>
              </footer>)
  }
}

Footer.dispatch = displayName
Footer.propTypes = propTypes

export default compose(
  withStyles(styles),
  withWidth()
)(Footer);
