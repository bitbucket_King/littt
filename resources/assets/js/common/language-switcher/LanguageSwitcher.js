import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux'
import { connect } from 'react-redux'
import MyButton from './../button/MyButton';

import { withStyles } from '@material-ui/core/styles';
import MenuList from "@material-ui/core/MenuList";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Grow from "@material-ui/core/Grow";
import Popper from "@material-ui/core/Popper";
import Grid from '@material-ui/core/Grid';
import MediaQuery from 'react-responsive';
import Collapse from '@material-ui/core/Collapse';
import MenuItem from '@material-ui/core/MenuItem';

import { Trans } from 'react-i18next';
import { FormattedMessage } from 'react-intl';

import { changeLanguage } from './../../modules/Intl/IntlActions'
const baseUrl = "/img/"

const styles = theme => ({
  root: {
    borderRadius: 25,
    padding: '40px 0px 20px 0',
    background: 'none',
    width: 650,
    boxShadow: 'none !important',
    backgroundImage: 'url(/img/panels/Panel3@1x.png)',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'left top',
    backgroundSize: '100% 100%',
  },
  topMenu: {
    color: 'white',
    fontSize: 23,
    fontWeight: 300,
    textTransform: 'none',
    padding: '20px 50px',
    [theme.breakpoints.down('md')]: {
      fontSize: 22,
      color: 'black',
      padding: 15,
      width: '100%',
    },
    outline: 'none !important;'
  },  
  paper: {
    marginRight: theme.spacing.unit * 2,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 120,
  },
  langBtn: {
    height: '100%',
    width: '10vw',
    maxWidth: 120,
    minWidth: 50
  }
});

class LanguageSwitcher extends React.Component {
    state = {
        auth: true,
        lanugage: 'English',
        flag: `${baseUrl}icons/flag/uk.svg`,
        open: false,
        mobileopen: false
    };

    changeLanguage = (lanugage, code, flag) => {
        this.props.changeLanguage(code)
        // this.props.dispatch(changeLanguage(code));
        this.setState({ lanugage: lanugage, flag: flag });
    }

    handleToggle = (event) => {
        event.preventDefault();
        this.setState(state => ({ open: !state.open }));
        {
            this.state.open?
            document.getElementById("border3").removeAttribute("class", "borderBottom3"): document.getElementById("border3").setAttribute("class", "borderBottom3");
        }
        {
            this.state.open && document.getElementById("footer")?
            document.getElementById("footer").removeAttribute("class", "fog"): document.getElementById("footer").setAttribute("class", "fog");
        }
    };

    handleClose = event => {
        if (this.anchorEl.contains(event.target)) {
            return;
        }
        document.getElementById("border3").removeAttribute("class", "borderBottom3");
        document.getElementById("footer") && document.getElementById("footer").removeAttribute("class", "fog");
        this.setState({ open: false });
    };

    handleMobileToggle = () => {
        this.setState(state => ({ mobileopen: !state.mobileopen }));
        {
            this.state.mobileopen  && document.getElementById("footer")?
            document.getElementById("footer").removeAttribute("class", "fog"): document.getElementById("footer").setAttribute("class", "fog");
        }
    };
    
    handleMobileClose = event => {
        if (this.anchorEl.contains(event.currentTarget)) {
            return;
        }
        document.getElementById("footer") && document.getElementById("footer").removeAttribute("class", "fog");
        this.setState({ mobileopen: false });
    };

    desktopLangSwitcher = (classes, open) =>{
        return (
        <div>
            <Button
                buttonRef={node => {
                    this.anchorEl = node;
                }}
                className={classes.langBtn}
                aria-owns={open ? 'menu-list-grow' : undefined}
                aria-haspopup="true"
                onClick={this.handleToggle}
            >
                <img src={this.state.flag} style={{marginRight: 20, width: 25}}/> <img src={`${baseUrl}icons/flag/drop-down.svg`} width="15%"/>
            </Button>
            <Popper open={open} anchorEl={this.anchorEl} transition disablePortal placement="bottom-end">
                {({ TransitionProps, placement }) => (
                <Grow
                    {...TransitionProps}
                    id="menu-list-grow"
                    style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                    >
                    <Paper style={{width: 140}}>
                        <ClickAwayListener onClickAway={this.handleClose}>
                        <MenuList>
                            <MenuItem onClick={(e) => { this.handleClose(e); this.changeLanguage('English', 'en',  `${baseUrl}icons/flag/uk.svg`)}}>
                            <img src={`${baseUrl}icons/flag/uk.svg`} style={{width: 25}}/>&nbsp;&nbsp;&nbsp; English
                            </MenuItem>
                            <MenuItem onClick={(e) => { this.handleClose(e); this.changeLanguage('Chinese', 'ch', `${baseUrl}icons/flag/china.svg`)}}>
                            <img src={`${baseUrl}icons/flag/china.svg`} style={{width: 25}}/>&nbsp;&nbsp;&nbsp; Chinese
                            </MenuItem>
                        </MenuList>
                        </ClickAwayListener>
                    </Paper>
                </Grow>
                )}
            </Popper>
        </div>
        )
    }

    mobileLangSwitcher = (classes, mobileopen) => {
        return (<Fragment>
            <Button
                buttonRef={node => { this.anchorEl = node; }}
                aria-owns={mobileopen ? "menu-list-grow" : null}
                aria-haspopup="true"
                onClick={this.handleMobileToggle}
                className={classes.topMenu}
            >
                <Trans i18nKey={this.props.menuTitle}></Trans>
            </Button>

            <Collapse in={this.state.mobileopen} timeout="auto" unmountOnExit 
                style={{
                    background: "linear-gradient(to left, rgb(169, 46, 145) 0%, rgb(181, 198, 52) 100%) left top / 100% 4px no-repeat rgb(255, 255, 255)",
                    boxShadow: 'inset 0px 0px 8px 0px rgba(125,125,125,1)',
                }}>
                <ClickAwayListener onClickAway={this.handleMobileClose}>
                    <MenuList style={{display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                            <MenuItem onClick={(e) => { this.handleMobileClose(e); this.changeLanguage('English', 'en',  `${baseUrl}icons/flag/uk.svg`)}}>
                            <img src={`${baseUrl}icons/flag/uk.svg`} style={{width: 25}}/>&nbsp;&nbsp;&nbsp; English
                            </MenuItem>
                            <MenuItem onClick={(e) => { this.handleMobileClose(e); this.changeLanguage('Chinese', 'ch', `${baseUrl}icons/flag/china.svg`)}}>
                            <img src={`${baseUrl}icons/flag/china.svg`} style={{width: 25}}/>&nbsp;&nbsp;&nbsp; Chinese
                            </MenuItem>
                    </MenuList>              
                </ClickAwayListener>
            </Collapse>
        </Fragment>)
    }

    render() {
        const { classes } = this.props;
        const { open, mobileopen } = this.state;//except "auth"

        return (
        <div id="border3">
        <MediaQuery query="(min-width: 992px)">
            {this.desktopLangSwitcher(classes, open)} 
        </MediaQuery>   
        <div style={{ 
                background: "linear-gradient( to left, rgba(169, 46, 145, 1) 0%, rgba(181, 198, 52, 1) 100% ) left bottom #fff no-repeat",
                backgroundSize: "100% 1px"
        }}>
            <MediaQuery query="(max-width: 991px)">
            {this.mobileLangSwitcher(classes, mobileopen)}
            </MediaQuery>
        </div> 
        </div>)        
    }
}

LanguageSwitcher.propTypes = {
    classes: PropTypes.object.isRequired,
    // handleClose: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
    menuTitle: PropTypes.string,
    subMenus: PropTypes.array,
    commonMenus: PropTypes.array  
};

const mapStateToProps = state => {
    return {
      locale: state.intl.locale,
    }
}
  
export default compose(
    withStyles(styles),
    connect(mapStateToProps)
)(LanguageSwitcher)