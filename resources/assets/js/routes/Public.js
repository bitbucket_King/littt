import React from 'react'
import PropTypes from 'prop-types'
import { Route } from 'react-router-dom'
import Layout from '../layout'
import BlogLayout from '../blogLayout'
import layout from '../layout';

const PublicRoutes = ({ component: Component, path, ...rest }) => {
  return <Route {...rest} render={props => {
      if(path.startsWith('/blog')) {
        return (<BlogLayout><Component {...props}/></BlogLayout>)
      }
      return (<Layout><Component {...props}/></Layout>)
    }
  }/>
}

PublicRoutes.propTypes = {
  component: PropTypes.func.isRequired,
  location: PropTypes.object,
};

export default PublicRoutes
