import React from 'react'
import PropTypes from 'prop-types'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
// import Main from '../Main'
import BlogLayout from '../blogLayout'
import layout from '../layout';

const PrivateRoute = ({ component: Component, isAuthenticated, path, ...rest }) => {
  return <Route {...rest} render={props => (
    isAuthenticated
      ? ( path.startsWith('/blog')?  (<BlogLayout><Component {...props}/></BlogLayout>) : (<Layout><Component {...props}/></Layout>) )
      : <Redirect to={{
        pathname: '/login',
        state: { from: props.location },
      }}/>
  )}/>
  
}

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  location: PropTypes.object,
  isAuthenticated: PropTypes.bool.isRequired,
}

// Retrieve data from store as props
function mapStateToProps(store) {
  return {
    isAuthenticated: store.auth.isAuthenticated,
  }
}

export default connect(mapStateToProps)(PrivateRoute)
